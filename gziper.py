#!/usr/bin/env  python
#-*- coding: utf-8 -*-

"""
* Support: Python 2.7
* Description: Main file for make gz archives
* Author: me@mosquito.su
* Part of: None
* Standalone: yes
"""

import re, os, sys, stat, sql, hashlib
import sqlite3
from ConfigParser import ConfigParser
from subprocess import Popen, PIPE

def readconfig(*args, **kwargs):
    cfg = ConfigParser()
    cfg_name = "config.cfg"
    if not os.path.isfile("config.cfg"):
        cfg.readfp(open("%s/%s" % (os.path.dirname(__file__), cfg_name),'r'))
    else:
        cfg.readfp(open("config.cfg",'r'))
    return cfg

class Flat:
    pass

def file_list(*paths):
    config = readconfig()
    filelist = list()
    rlist = list()
    for ext, rul in config.items('ignore_ext'):
        if rul == 'true':
            rlist.append(re.compile(".*\.%s$" % ext.replace(".", r'\.')))

    for path in paths:
        for root, subFolders, files in os.walk(path):
            for file in files:
                rstop = False
                f = os.path.join(root,file)
                try:
                    t = open(f, 'rb')
                    t.close()
                except:
                    sys.stderr.write("Not access in: %s\n" % f)
                    continue

                for rexp in rlist:
                    if rexp.match(f) != None:
                        rstop = True
                if rstop:
                    continue

                t = Flat()
                t.stat = os.stat(f)
                t.path = f
                filelist.append(t)
    sys.stderr.flush()
    return filelist

def db_connect():
    name = 'database.sqlite'
    if not os.path.isfile(name):
        db_name = "%s/%s" % (os.path.dirname(__file__), name)
    else:
        db_name = name

    db = sqlite3.connect(db_name)

    c = sql.select("SELECT sql FROM sqlite_master WHERE name='files'", connect=db).objects
    q = "CREATE TABLE files (id INTEGER PRIMARY KEY, path TEXT, change INTEGER DEFAULT 0, mtime REAL, size INTEGER, md5 TEXT)"
    if not len(c):
        sql.query(q, connect=db, commit=False).save()
        sql.query("CREATE UNIQUE INDEX files_path_uniq ON files(path)", connect=db, commit=False).save()
        db.commit()
    else:
        if c[0].sql != q:
            raise Exception("Invalid table structure")

    return db

def md5_file(fp, block_size=2**20):
    if isinstance(fp, str):
        f = open(fp, "rb")
    else:
        f = fp

    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    return md5.hexdigest()

if __name__ == "__main__":
    db = db_connect()

    config = readconfig()
    files = file_list(*[i[1] for i in config.items('path')])

    updated_files = list()

    for f in files:
        o = sql.select("SELECT mtime, size, change, md5 FROM files WHERE path='%s'" % f.path, connect=db).objects
        if not len(o):
            print "File '%s' not in database" % f.path
            sql.query("INSERT INTO files (path, md5, mtime, size) VALUES ('%s', '%s', '%s', '%s')" % (
                            f.path.replace("'", r"\'"),
                            md5_file(f.path),
                            f.stat.st_mtime,
                            f.stat.st_size
                            ), commit=False, connect=db).save()
        else:
            o = o[0]
            change = False
            if int(f.stat.st_size) != int(o.size) or "%.2f" % float(f.stat.st_mtime) != "%.2f" % float(o.mtime):
                sql.query("UPDATE files SET change='1' WHERE path='%s'" % (f.path), commit=False, connect=db).save()
                change = True
            sql.query("UPDATE files SET mtime='%s' WHERE path='%s'" % (f.stat.st_mtime, f.path), commit=False, connect=db).save()
            sql.query("UPDATE files SET size='%s' WHERE path='%s'" % (f.stat.st_size, f.path), commit=False, connect=db).save()

            if change and o.change:
                print "File changes now", f.path

            elif change and not o.change:
                print "File changed", f.path
            elif o.change and (int(f.stat.st_size) == int(o.size) and "%.2f" % float(f.stat.st_mtime) == "%.2f" % float(o.mtime)):
                md5 = md5_file(f.path)
                if str(o.md5) != str(md5):
                    sql.query("UPDATE files SET md5='%s' WHERE path='%s'" % (md5, f.path), commit=False, connect=db).save()
                    print "File updated %s, new md5 %s" % (f.path, md5)
                    updated_files.append(f.path)

                sql.query("UPDATE files SET change='0' WHERE path='%s'" % (f.path), commit=False, connect=db).save()

            if not o.change and not change:
                if not os.path.isfile("%s.gz" % f.path):
                    updated_files.append(f.path)

    db.commit()

    for f in updated_files:
        print "Archiving: %s" % f
        cmd = Popen("""gzip -9c "%(path)s" > "%(path)s.gztmp" """ % { 'path': f }, shell=True, stderr=PIPE)
        err = cmd.stderr.read()
        if not len(err):
            if os.path.isfile("%s.gztmp" % f):
                try:
                    os.rename("%s.gztmp" % f, "%s.gz" % f)
                except:
                    pass
                try:
                    os.chmod("%s.gz" % f, int("666",8))
                    print "Changes rules"
                except:
                    pass
        else:
            print err
            if os.path.isfile("%s.gztmp" % f):
                os.remove("%s.gztmp" % f)
    exit(0)


