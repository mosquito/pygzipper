#-*- coding: utf-8 -*-

import re

class sqlitem:
    def __init__(self, *args, **kwargs):
        self.__dict__=dict()
        for key, value in kwargs.items():
            setattr(self, key, value)
            self.__dict__[key]=value
    def __str__(self):
        S="\n"
        for key, value in self.__dict__.items():
            if type(value) == str or type(value) == unicode:
                value = value.encode('utf-8')
            S += "\t"+str(key)+":\t"+str(value)+"\n"
        return S+"\n"

    def __unicode__(self):
        self.__str__()

class select:
    def __init__(self, query, cursor=None, connect=None):
        self.__close_cur = False
        if cursor == None and connect != None:
            self.__cursor = connect.cursor()
            self.__close_cur = True
        elif cursor != None:
            self.__cursor = cursor

        self.__cursor.execute(query)
        fetch = self.__cursor.fetchall()
        fields = [re.sub(r'[\(\)\+\-\.\;\n\"\'\*\/\\\%\|\@\#\$\^\!\=\[\]\{\}\,\~\`\?\<\>]', '_', r[0]) for r in self.__cursor.description]
        row = list()
        self.__keys = list()
        if not len(fetch)==0:
            for i in fetch:
                keys = dict()
                for k in range(0,len(fields)):
                    keys[fields[k]]=i[k]
                row.append(sqlitem(**keys))
                self.__keys.append(keys)
        self.objects = row

    def __del__(self):
        if self.__close_cur:
            self.__cursor.close()

class query(select):
    def __init__(self, query, cursor=None, connect=None, commit=False):
        self.__close_cur = False
        if cursor == None and connect != None:
            self.__cursor = connect.cursor()
            self.__close_cur = True
        elif cursor != None:
            self.__cursor = cursor
        self.__query = query
        self.__connection = connect
        self.__commit=commit

    def save(self):
        self.__cursor.execute(self.__query)
        if self.__commit:
            self.__connection.commit()

    def __str__(self):
        return str(self.__query)

    def __unicode__(self):
        return unicode(self.__query)

    def __del__(self):
        if self.__close_cur:
            self.__cursor.close()
